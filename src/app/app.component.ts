import {Component, OnInit, ViewChild} from "@angular/core";
import {TabsetComponent} from "ngx-bootstrap/tabs";
import {AppBlService} from "./app-bl.service";

@Component({
  selector: "app-root",
  template: `
    <div class="container">
      <app-navbar></app-navbar>
      <div class="logoArea">
      </div>
      <div class="mainContentArea">
        <router-outlet></router-outlet>

      </div>
    </div>

    <p-toast></p-toast>

  `,
  styles: [``]
})
export class AppComponent implements OnInit {
  @ViewChild("staticTabs")
  staticTabs: TabsetComponent;

  constructor(private appBlService: AppBlService) {
  }

  ngOnInit(): void {
    this.appBlService.initData();
  }


  selectTab(tab_id: number) {
    this.staticTabs.tabs[tab_id].active = true;
  }
}
