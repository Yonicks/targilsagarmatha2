import {User} from './shared/classes/user.model';
import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from '../../node_modules/rxjs';
import {Order} from './shared/classes/order.model';
import {Constants} from './shared/constants';
import * as _ from 'lodash';

@Injectable()
export class AppBlService {
  constructor() {
  }

  public _users: BehaviorSubject<User[]> = new BehaviorSubject<User[]>([]);
  public _orders: BehaviorSubject<Order[]> = new BehaviorSubject<Order[]>([]);

  initData() {
    this.getUsers();
    this.getOrders();
  }

  // USERS
  getUsers() {
    this.setUsers(this.getUsersLocalStorage());
  }

  get users(): Observable<User[]> {
    return this._users.asObservable();
  }

  setUsers(users: User[]): void {
    users = users ? _.cloneDeep(users) : [];
    this._users.next(users);
  }

  addUser(user: User): void {
    let currentUsers: User[] = this.getCurrentUsersArray();
    currentUsers.push(user);
    this.setUsers(currentUsers);
  }

  updateUser(user: User): void {
    let currentUsers: User[] = this.getCurrentUsersArray();
    let indexToUpdate = currentUsers.findIndex(u => u.id == user.id);
    currentUsers[indexToUpdate] = user;
    this.setUsers(currentUsers);
  }

  deleteUser(user: User): void {
    let currentUsers: User[] = this.getCurrentUsersArray();
    let indexToUpdate = currentUsers.findIndex(u => u.id == user.id);
    currentUsers.splice(indexToUpdate, 1);
    this.setUsers(currentUsers);
  }

  getCurrentUsersArray(): User[] {
    return this._users.value as User[];
  }

  setUsersLocalStorage(users: User[]) {
    localStorage.setItem(Constants.LS_USERS, JSON.stringify(users));
  }

  getUsersLocalStorage(): User[] {
    let users: User[] = [];
    try {
      const usersJson = localStorage.getItem(Constants.LS_USERS);
      users = JSON.parse(usersJson);
      // fake http request
      setTimeout(() => {
        users = users == null ? [] : users;
      });
    }
    catch (ex) {
      console.log('error:' + ex);
    }
    return users;
  }

  // ORDERS
  getOrders() {
    this.setOrders(this.getOrdersLocalStorage());
  }

  get orders(): Observable<Order[]> {
    return this._orders.asObservable();
  }

  setOrders(orders: Order[]) {
    orders = orders ? _.cloneDeep(orders) : [];
    this._orders.next(orders);
  }

  addOrder(order: Order) {
    let currentOrders: Order[] = this.getCurrentOrdersArray();
    currentOrders.push(order);
    this.setOrders(currentOrders);
  }

  updateOrder(order: Order): void {
    let currentOrders: Order[] = this.getCurrentOrdersArray();
    let indexToUpdate = currentOrders.findIndex(o => o.id == order.id);
    currentOrders[indexToUpdate] = order;
    this.setOrders(currentOrders);
  }

  deleteOrder(order: Order) {
    let currentOrders: Order[] = this.getCurrentOrdersArray();
    let indexToUpdate = currentOrders.findIndex(o => o.id == order.id);
    currentOrders.splice(indexToUpdate, 1);
    this.setOrders(currentOrders);
  }

  deleteOrdersByUserId(userId: string) {
    let userOrders: Order[] = this._orders.value.filter(o => o.userId == userId);
    userOrders.forEach(uo => this.deleteOrder(uo));
  }

  getCurrentOrdersArray(): Order[] {
    return this._orders.value as Order[];
  }

  setOrdersLocalStorage(orders: Order[]) {
    localStorage.setItem(Constants.LS_ORDERS, JSON.stringify(orders));
  }

  getOrdersLocalStorage(): Order[] {
    let orders: Order[] = [];
    try {
      const ordersJson = localStorage.getItem(Constants.LS_ORDERS);
      orders = JSON.parse(ordersJson);

      // fake http request
      setTimeout(() => {
        orders = orders == null ? [] : orders;
      });
    }
    catch (ex) {
      console.log('error:' + ex);
    }
    return orders;
  }
}
