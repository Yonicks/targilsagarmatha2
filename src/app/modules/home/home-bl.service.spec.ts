import { TestBed, inject } from '@angular/core/testing';

import { HomeBlService } from './home-bl.service';

describe('HomeBlService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HomeBlService]
    });
  });

  it('should be created', inject([HomeBlService], (service: HomeBlService) => {
    expect(service).toBeTruthy();
  }));
});
