import {ChangeDetectionStrategy, Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {ModeTypeEnum} from '../../shared/enums/mode-type.enum';
import {Order} from '../../shared/classes/order.model';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {MessageService} from 'primeng/api';
import {OrdersBlService} from './orders-bl.service';
import {AppBlService} from '../../app-bl.service';
import {User} from '../../shared/classes/user.model';
import {Subscription} from 'rxjs/internal/Subscription';
import {Observable} from 'rxjs';
import {Constants} from '../../shared/constants';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `

    <page-header [title]="ORDERS" [isShowAddBtn]="!orderModeType"
                 (addClicked)="changeAddEditMode(modeTypes.add)"></page-header>

    <orders-add-edit *ngIf="orderModeType == modeTypes.add"
                     [mode]="orderModeType"
                     [usersList]="usersList"
                     (doneClicked)="saveOrderChanges($event)"
                     (backClicked)="changeAddEditMode(null)"></orders-add-edit>

    <app-orders-list [orderList]="orders$ | async"
                     [usersList]="usersList"
                     (editClicked)="editClicked($event)"
                     (deleteClicked)="deleteClicked($event)"></app-orders-list>


    <ng-template #templateAddEdit>
      <div class="modal-header">
        <h4 class="modal-title pull-left">Edit Order</h4>
        <button type="button" class="close pull-right" aria-label="Close" (click)="modalRef.hide()">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" *ngIf="orderModeType === modeTypes.edit">
        <orders-add-edit *ngIf="orderModeType == modeTypes.edit"
                         [order]="orderOnEdit"
                         [usersList]="usersList"
                         [mode]="orderModeType"
                         (doneClicked)="saveOrderChanges($event)"
                         (backClicked)="changeAddEditMode(null)"></orders-add-edit>
      </div>
    </ng-template>


    <ng-template #templateDelete>
      <popup-are-you-sure *ngIf="orderOnDelete" [id]="orderOnDelete.id" [typeData]="ORDER"
                          (confirm)="confirm()" (decline)="decline()"></popup-are-you-sure>
    </ng-template>
  `,
  styles: [``]
})

export class OrdersComponent implements OnInit, OnDestroy {
  orderOnEdit: Order;
  orderOnDelete: Order;
  orderModeType: ModeTypeEnum;
  modeTypes = ModeTypeEnum;
  modalRef: BsModalRef;
  usersList: User[] = [];
  @ViewChild('templateAddEdit') templateAddEdit;
  @ViewChild('templateDelete') templateDelete;

  ORDERS: string = Constants.ORDERS;
  ORDER: string = Constants.ORDER;

  _orderMode_subsc: Subscription;
  _orderOnEdit_subsc: Subscription;
  _orderOnDelete_subsc: Subscription;
  onHide_subsc: Subscription;
  _orders_subsc: Subscription;
  orders$: Observable<Order[]>;


  constructor(public ordersBlService: OrdersBlService,
              private modalService: BsModalService,
              public appBlService: AppBlService,
              private messageService: MessageService) {
  }

  ngOnInit() {

    this.orders$ = this.appBlService.orders;

    this.usersList = this.appBlService.getCurrentUsersArray();

    this._orderMode_subsc = this.ordersBlService._orderMode.subscribe((status: ModeTypeEnum) => {
      this.orderModeType = status;
    });
    this._orderOnDelete_subsc = this._orderOnEdit_subsc = this.ordersBlService._orderOnEdit.subscribe((order: Order) => {
      this.orderOnEdit = order;
    });
    this.ordersBlService._orderOnDelete.subscribe((order: Order) => {
      this.orderOnDelete = order;
    });

    this.onHide_subsc = this.modalService.onHide.subscribe(() => {
      this.ordersBlService.updateOrderMode(null);
    });


    this._orders_subsc = this.appBlService._orders.subscribe(orders => {
      // for each order find it User object
      orders.forEach(order => {
        order.user = this.appBlService._users.value.find(u => u.id == order.userId);
      });

      // when orders list changing -> update local storage
      this.appBlService.setOrdersLocalStorage(orders);
    });


  }

  ngOnDestroy() {
    this._orderMode_subsc.unsubscribe();
    this._orderOnEdit_subsc.unsubscribe();
    this._orderOnDelete_subsc.unsubscribe();
    this.onHide_subsc.unsubscribe();
    this._orders_subsc.unsubscribe();
  }


  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  editClicked(order: Order) {
    this.ordersBlService.updateOrderOnEdit(order);
    this.ordersBlService.updateOrderMode(ModeTypeEnum.edit);
    this.openModal(this.templateAddEdit);
  }

  deleteClicked(order: Order) {
    this.ordersBlService.updateOrderOnDelete(order);
    this.openModal(this.templateDelete);
  }

  saveOrderChanges(order: Order) {
    if (!this.ordersBlService.isOrderValid(order)) {
      this.messageService.add({severity: 'error', summary: 'Order not valid', detail: 'not valid'});
      return;
    }
    if (this.orderModeType == ModeTypeEnum.edit) {
      this.modalRef.hide();
    }
    this.ordersBlService.preformAddEditOrder(order);

  }

  changeAddEditMode(status: ModeTypeEnum) {
    // if on edit mode => close
    if (this.orderModeType == ModeTypeEnum.edit)
      this.modalRef.hide();
    this.ordersBlService.updateOrderMode(status);
  }

  confirm() {
    this.ordersBlService.preformOrderDelete(this.orderOnDelete);
    this.modalRef.hide();
  }

  decline() {
    this.modalRef.hide();
    this.ordersBlService.updateOrderOnDelete(null);
  }
}
