import {Injectable} from "@angular/core";
import {Order} from "../../shared/classes/order.model";
import {MessageService} from "primeng/api";
import {AppBlService} from "../../app-bl.service";
import {BehaviorSubject} from "rxjs/index";
import {ModeTypeEnum} from "../../shared/enums/mode-type.enum";

@Injectable()
export class OrdersBlService {

  public _orderMode: BehaviorSubject<ModeTypeEnum> = new BehaviorSubject<ModeTypeEnum>(null);
  public _orderOnEdit: BehaviorSubject<Order> = new BehaviorSubject<Order>(null);
  public _orderOnDelete: BehaviorSubject<Order> = new BehaviorSubject<Order>(null);


  constructor(private appBlService: AppBlService,
              private messageService: MessageService) {
  }

  updateOrderMode(modeType: ModeTypeEnum) {
    this._orderMode.next(modeType);
  }


  updateOrderOnEdit(order: Order) {
    this._orderOnEdit.next(order);
  }

  updateOrderOnDelete(order: Order) {
    this._orderOnDelete.next(order);
  }


  public preformAddEditOrder(order: Order): boolean {
    if (this._orderMode.value == ModeTypeEnum.add) {
      this.appBlService.addOrder(order);
      this.messageService.add({
        severity: 'success',
        summary: 'Order Added Successful',
        detail: 'Order name:' + order.name
      });
    }
    else if (this._orderMode.value == ModeTypeEnum.edit) {
      this.appBlService.updateOrder(order);
      this.messageService.add({
        severity: 'success',
        summary: 'Order Updated Successful',
        detail: 'Order name:' + order.name
      });
    }
    this.updateOrderMode(null);

    return true;

  }


  public preformOrderDelete(order: Order) {
    this.appBlService.deleteOrder(order);
    this.updateOrderMode(null);
    this.messageService.add({
      severity: 'success',
      summary: 'Order deleted Successful',
      detail: 'Order name:' + order.name
    });
  }


  isOrderValid(order: Order): boolean {
    let isValid = (order.name.length < 4) || (!order.userId) ? false : true;

    return isValid;
  }


}
