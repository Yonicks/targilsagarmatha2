import {Routes, RouterModule} from "@angular/router";
import {IRouting} from "../../shared/interfaces/IRouting";
import {OrdersComponent} from "./orders.component";

const routes: Routes = [
  {path: '', component: OrdersComponent},
  {path: ':id', component: OrdersComponent},
  {path: '**', redirectTo: ''}
]

export const OrdersRouting: IRouting = {
  routes: RouterModule.forChild(routes),
  components: [
    OrdersComponent
  ]
};
