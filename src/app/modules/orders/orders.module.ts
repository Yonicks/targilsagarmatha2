import {NgModule} from '@angular/core';

import {OrdersComponent} from './orders.component';
import {RouterModule} from '@angular/router';
import {OrdersRouting} from './orders.routing';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {SharedModule} from '../shared/shared.module';
import {OrdersBlService} from './orders-bl.service';
import {OrdersListComponent} from "./components/orders-list.component";
import {OrdersAddEditComponent} from './components/orders-add-edit.component';

@NgModule({
  imports: [RouterModule, OrdersRouting.routes, CommonModule, FormsModule,
    SharedModule],
  exports: [],
  declarations: [OrdersRouting.components, OrdersComponent, OrdersListComponent, OrdersAddEditComponent],
  providers: [OrdersBlService],
})
export class OrdersModule {
}
