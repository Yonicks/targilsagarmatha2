import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import {Constants} from '../../../shared/constants';
import {Order} from '../../../shared/classes/order.model';
import {MatSort, MatTableDataSource} from '@angular/material';
import {ActivatedRoute} from '@angular/router';
import {User} from '../../../shared/classes/user.model';
import {Subscription} from 'rxjs/internal/Subscription';

@Component({
  selector: 'app-orders-list',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div class="dataTable">
      <table class="dataTable" mat-table [dataSource]="dataSource" matSort class="mat-elevation-z8">

        <!-- Id Column -->
        <ng-container matColumnDef="id">
          <th mat-header-cell *matHeaderCellDef mat-sort-header> Id</th>
          <td mat-cell *matCellDef="let element"> {{element.id}}</td>
        </ng-container>

        <!-- Name Column -->
        <ng-container matColumnDef="name">
          <th mat-header-cell *matHeaderCellDef mat-sort-header>Order Name</th>
          <td mat-cell *matCellDef="let element"> {{element.name}}</td>
        </ng-container>


        <!-- user name Column -->
        <ng-container matColumnDef="userName">
          <th mat-header-cell *matHeaderCellDef mat-sort-header>User Name</th>
          <td mat-cell *matCellDef="let element"> {{element.user.name}}</td>
        </ng-container>

        <!-- Star Column -->
        <ng-container matColumnDef="star" stickyEnd>
          <th mat-header-cell *matHeaderCellDef></th>
          <td class="btnArea" mat-cell *matCellDef="let element">
            <i class="fa fa-edit" [tooltip]="editOrder" (click)="editClicked.emit(element)"></i>
            <i class="fa fa-trash" [tooltip]="deleteOrder" (click)="deleteClicked.emit(element)"></i>
          </td>
        </ng-container>


        <tr mat-header-row *matHeaderRowDef="displayedColumns"></tr>
        <tr mat-row *matRowDef="let row; columns: displayedColumns;"></tr>
      </table>
    </div>
  `,
  styles: [`
  `]
})
export class OrdersListComponent implements OnInit, OnDestroy, OnChanges {
  displayedColumns: string[] = ['id', 'name', 'userName', 'star'];
  dataSource;
  @Output() editClicked: EventEmitter<Order> = new EventEmitter<Order>();
  @Output() deleteClicked: EventEmitter<Order> = new EventEmitter<Order>();
  @Input() orderList: Order[];
  @Input() usersList: User[];

  public editOrder: string = Constants.EDIT_ORDER;
  public deleteOrder: string = Constants.DELETE_ORDER;
  private userIdToFilterBy: string;

  @ViewChild(MatSort)
  sort: MatSort;

  params_subsc: Subscription;

  constructor(private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.params_subsc = this.route.params.subscribe(data => {

    });
  }

  ngOnDestroy() {
    this.params_subsc.unsubscribe();
  }

  setDataSource(orders: Order[]) {
    this.userIdToFilterBy = this.route.snapshot.params['id'];
    let ordersListToUse: Order[] = orders;
    if (this.userIdToFilterBy) {
      ordersListToUse = ordersListToUse.filter(o => o.userId == this.userIdToFilterBy);
    }
    this.dataSource = new MatTableDataSource(ordersListToUse);
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.setDataSource(this.orderList);
  }


}
