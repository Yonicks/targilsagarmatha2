import {Component, OnInit, Output, Input, EventEmitter, OnChanges, ChangeDetectionStrategy} from "@angular/core";
import {Order} from "../../../shared/classes/order.model";
import {ModeTypeEnum} from "../../../shared/enums/mode-type.enum";
import * as _ from 'lodash';
import {User} from "../../../shared/classes/user.model";

@Component({
  selector: 'orders-add-edit',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div class="addOrderArea py-5">
      <button class="btn btn-info" (click)="backClicked.emit()">BACK</button>
      <h1>{{title}}-order</h1>

      <form class="example-form">
        <mat-form-field class="example-full-width">
          <input matInput placeholder="Id" disabled [(ngModel)]="orderMock.id" name="id">
        </mat-form-field>

        <mat-form-field class="example-full-width">
          <input matInput placeholder="Name" [(ngModel)]="orderMock.name" name="name"/>
        </mat-form-field>

        <mat-form-field>
          <mat-select placeholder="users" [(value)]="orderMock.userId">
            <mat-option *ngFor="let user of usersList" [value]="user.id">
              {{user.name}}
            </mat-option>
          </mat-select>
        </mat-form-field>
      </form>

      <button class="btn btn-primary" (click)="doneClicked.emit(orderMock)"
              [class.disabled]="orderMock.name.length < 4">Save
      </button>
    </div>
  `,
  styles: [`
    .example-form {
      min-width: 150px;
      max-width: 500px;
      width: 100%;
    }

    .example-full-width {
      width: 100%;
    }
  `]
})
export class OrdersAddEditComponent implements OnInit, OnChanges {
  @Output() backClicked = new EventEmitter<any>();
  @Output() doneClicked = new EventEmitter<Order>();
  @Input() order: Order;
  orderMock: Order;
  @Input() mode: ModeTypeEnum;
  @Input() usersList: User[];
  title: string;

  constructor() {
  }

  ngOnInit() {
    if (this.mode == ModeTypeEnum.add) {
      this.orderMock = new Order();
    } else {
      this.orderMock = _.cloneDeep(this.order);
    }
  }

  ngOnChanges(changes) {
    this.title = this.mode.toString();
  }

}
