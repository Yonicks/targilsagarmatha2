import { Routes, RouterModule } from "@angular/router";
import { IRouting } from "../../shared/interfaces/IRouting";
import { BaseComponent } from "./base.component";

const routes: Routes = [
    { path: '', component: BaseComponent },
    { path: '**', redirectTo: '' }
]

export const BaseRouting: IRouting = {
    routes: RouterModule.forChild(routes),
    components: [
        BaseComponent
    ]
};