import { NgModule } from '@angular/core';

import { BaseComponent } from './base.component';
import { RouterModule } from '@angular/router';
import { BaseRouting } from './base.routing';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { BaseBlService } from './base-bl.service';
import { BaseFunctionsService } from './base-functions.service';

@NgModule({
    imports: [RouterModule, BaseRouting.routes, CommonModule, FormsModule,
        SharedModule],
    exports: [],
    declarations: [BaseRouting.components],
    providers: [BaseBlService, BaseFunctionsService],
})
export class BaseModule { }
