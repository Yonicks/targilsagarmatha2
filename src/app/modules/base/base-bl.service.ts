import { Injectable } from '@angular/core';
import { GlobalFuncsService } from '../../shared/global-funcs.service';

@Injectable()
export class BaseBlService {

    constructor(
        private globalFuncsService: GlobalFuncsService
    ) { }
}