import {AlertModule} from "ngx-bootstrap/alert";
import {ButtonsModule} from "ngx-bootstrap/buttons";
import {AgGridModule} from "ag-grid-angular";
import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {BsDatepickerModule} from "ngx-bootstrap/datepicker";
import {AngularFontAwesomeModule} from "angular-font-awesome";
import {MaterialModule} from "../material/material.module";
import {BsDropdownModule, CollapseModule, ModalModule, TabsModule, TooltipModule} from "ngx-bootstrap";
import {TrimPipe} from "./pipes/trim.pipe";
import {ModuleWithProviders} from "@angular/compiler/src/core";
import {PageHeaderComponent} from './components/page-header.component';
import {PopupAreYouSureComponent} from "./components/popup-are-you-sure.component";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    AngularFontAwesomeModule,
    AlertModule.forRoot(),
    AgGridModule.withComponents([]),
    ButtonsModule.forRoot(),
    BsDatepickerModule.forRoot(),
    MaterialModule,
    TabsModule.forRoot(),
    CollapseModule.forRoot(),
    BsDropdownModule.forRoot(),
    TooltipModule.forRoot(),
    ModalModule.forRoot()
  ],
  declarations: [TrimPipe, PageHeaderComponent, PopupAreYouSureComponent],
  exports: [
    CommonModule,
    FormsModule,
    AngularFontAwesomeModule,
    AlertModule,
    AgGridModule,
    ButtonsModule,
    BsDatepickerModule,
    TabsModule,
    CollapseModule,
    BsDropdownModule,
    MaterialModule,
    TooltipModule,
    TrimPipe,
    ModalModule,
    PageHeaderComponent,
    PopupAreYouSureComponent
  ]
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: []
    };
  }

}
