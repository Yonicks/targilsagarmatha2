import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'page-header',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <h1>{{title}}</h1>

    <button class="btn btn-primary mb-3" *ngIf="isShowAddBtn" (click)="addClicked.emit()">
      ADD
    </button>
  `,
  styles: []
})
export class PageHeaderComponent implements OnInit {
  @Input() title: string;
  @Input() isShowAddBtn: boolean;
  @Output() addClicked: EventEmitter<any> = new EventEmitter<any>();

  constructor() {
  }

  ngOnInit() {
  }

}
