import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Constants} from '../../../shared/constants';

@Component({
  selector: 'popup-are-you-sure',
  template: `
    <div class="modal-body text-center">
      <p>{{ARE_YOU_SURE_DELETE + ' ' + typeData + ' ' + id}}?</p>
      <button type="button" class="btn btn-default" (click)="confirm.emit()">{{YES}}</button>
      <button type="button" class="btn btn-primary" (click)="decline.emit()">{{NO}}</button>
    </div>
  `,
  styles: []
})
export class PopupAreYouSureComponent implements OnInit {
  ARE_YOU_SURE_DELETE: string = Constants.ARE_YOU_SURE_DELETE;
  YES: string = Constants.YES;
  NO: string = Constants.NO;
  @Input() typeData: string;
  @Input() id: string;
  @Output() confirm: EventEmitter<any> = new EventEmitter<any>();
  @Output() decline: EventEmitter<any> = new EventEmitter<any>();

  constructor() {
  }

  ngOnInit() {
  }

}
