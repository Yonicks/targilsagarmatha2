import { ApisService } from "./../../shared/proxies/ApisService";
import { BrowserModule } from "@angular/platform-browser";
import { FormsModule } from "@angular/forms";
import { NgModule, Optional, SkipSelf, APP_INITIALIZER } from "@angular/core";

import { CoreRoutingModule } from "./core-routing.module";
import { ConfigService } from "./services/config.service";
import { HttpService } from "./services/http.service";
import { environment } from "../../../environments/environment.prod";
import { HttpClientModule } from "@angular/common/http";
import { GlobalFuncsService } from "../../shared/global-funcs.service";
import { EnsureModuleLoadedOnceGuard } from "../../shared/ensureModuleLoadedOnceGuard";

export function ConfigLoader(configService: ConfigService) {
  return () => configService.load(environment.configFile);
}

@NgModule({
  imports: [FormsModule, HttpClientModule, CoreRoutingModule, BrowserModule],
  exports: [FormsModule, HttpClientModule,],
  declarations: [],
  providers: [
    ConfigService,
    GlobalFuncsService,
    HttpService,
    ApisService,
    {
      provide: APP_INITIALIZER,
      useFactory: ConfigLoader,
      deps: [ConfigService],
      multi: true
    }
  ]
})
export class CoreModule extends EnsureModuleLoadedOnceGuard {
  /* make sure CoreModule is imported only by one NgModule the AppModule */
  constructor(
    @Optional()
    @SkipSelf()
    parentModule: CoreModule
  ) {
    super(parentModule);
  }
}
