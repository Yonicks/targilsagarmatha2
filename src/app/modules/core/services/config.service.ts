import {Injectable} from "@angular/core";
import {Configuration} from "../../../shared/configuration";
import {Observable} from "rxjs";
import {switchMap} from "rxjs/operators";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class ConfigService {
  private config: Configuration;

  constructor(private http: HttpClient) {}

  load(url: string) {
    return new Promise(resolve => {
      this.http
        .get<Configuration>(url)
        .pipe(
          switchMap((config: Configuration) => {
            this.config = config;
            return this.getVersion(this.config);
          })
        )
        .subscribe(data => {
          this.config.data = data;
          resolve();
        });
    });
  }

  private getVersion(config2: Configuration): Observable<string> {
    let data;
    // return of ("");
    return Observable.create(observer => {
      this.http
        .get(window.location.origin, { responseType: "text" })
        .subscribe(res => {
          //data = res.headers.get("data");
          if (data == null) {
            if (config2.data != null) {
              data = config2.data;
            } else {
              data = null;
            }
            observer.next(data);
            observer.complete();
          }
        });
    });
  }

  getConfiguration(): Configuration {
    return this.config;
  }
}
