import { catchError } from "rxjs/operators";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpResponse } from "@angular/common/http";
import { Response } from "@angular/http";
import { Observable } from "rxjs";
import { ConfigService } from "./config.service";
import { GlobalFuncsService } from "../../../shared/global-funcs.service";

@Injectable()
export class HttpService {
  constructor(
    private _httpClient: HttpClient,
    private configService: ConfigService,
    private globalFuncsService: GlobalFuncsService
  ) {}

  get<T>(apiName: string, urlForReq?: string): Observable<T> {
    const urlToUse =
      urlForReq != null
        ? urlForReq
        : this.configService.getConfiguration().baseUrl;
    return this._httpClient
      .get<T>(urlToUse + apiName)
      .pipe(catchError(error => this.handleError(error)));
  }

  post<T>(data: any, apiName: string, urlForReq?: string): Observable<T> {
    const urlToUse =
      urlForReq != null
        ? urlForReq
        : this.configService.getConfiguration().baseUrl;
    const encoded_data = JSON.stringify({ data });
    const headers = new HttpHeaders({
      "Content-Type": "application/json;charset=utf-8"
    });
    return this._httpClient
      .post<T>(urlToUse + apiName, encoded_data, { headers: headers })
      .pipe(catchError(error => this.handleError(error)));
  }

  private handleError(error: HttpResponse<any> | any) {
    let errMsg: string;
    if (error instanceof HttpResponse) {
      const body = error.body || "";
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText} || ''} `;
    } else {
      errMsg = error.massage ? error.massage : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

  // ASYNC

  getAsync(apiUrl): Promise<any> {
    return this._httpClient
      .get(this.configService.getConfiguration().baseUrl + apiUrl)
      .toPromise();
  }
}
