import {Injectable} from '@angular/core';
import {AppBlService} from '../../app-bl.service';
import {User} from '../../shared/classes/user.model';
import {MessageService} from 'primeng/api';
import {BehaviorSubject} from 'rxjs/index';
import {ModeTypeEnum} from '../../shared/enums/mode-type.enum';

@Injectable()
export class UsersBlService {
  // public _isOnAddMode: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  // public _isOnEditMode: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  public _userMode: BehaviorSubject<ModeTypeEnum> = new BehaviorSubject<ModeTypeEnum>(null);
  public _userOnEdit: BehaviorSubject<User> = new BehaviorSubject<User>(null);
  public _userOnDelete: BehaviorSubject<User> = new BehaviorSubject<User>(null);


  constructor(private appBlService: AppBlService,
              private messageService: MessageService) {
  }

  updateUserMode(modeType: ModeTypeEnum) {
    this._userMode.next(modeType);
  }

  updateUserOnEdit(user: User) {
    this._userOnEdit.next(user);
  }

  updateUserOnDelete(user: User) {
    this._userOnDelete.next(user);
  }

  public preformAddEditUser(user: User): boolean {
    if (this._userMode.value == ModeTypeEnum.add) {
      this.appBlService.addUser(user);
      this.messageService.add({
        severity: 'success',
        summary: 'User Added Successful',
        detail: 'User name:' + user.name
      });
    }
    else if (this._userMode.value == ModeTypeEnum.edit) {
      this.appBlService.updateUser(user);
      this.messageService.add({
        severity: 'success',
        summary: 'User Updated Successful',
        detail: 'User name:' + user.name
      });
    }
    this.updateUserMode(null);

    return true;

  }

  public preformUserDelete(user: User) {
    this.appBlService.deleteUser(user);
    this.appBlService.deleteOrdersByUserId(user.id);
    this.updateUserMode(null);
    this.messageService.add({
      severity: 'success',
      summary: 'User deleted Successful',
      detail: 'User name:' + user.name
    });
  }

  isUserValid(user: User): boolean {
    const isValid = user.name.length < 4 ? false : true;
    return isValid;
  }


}
