import {Routes, RouterModule} from "@angular/router";
import {IRouting} from "../../shared/interfaces/IRouting";
import {UsersComponent} from "./users.component";

const routes: Routes = [
  {path: '', component: UsersComponent},
  {path: '**', redirectTo: ''}
]

export const UsersRouting: IRouting = {
  routes: RouterModule.forChild(routes),
  components: [
    UsersComponent
  ]
};
