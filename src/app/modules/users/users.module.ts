import {NgModule} from '@angular/core';

import {UsersComponent} from './users.component';
import {RouterModule} from '@angular/router';
import {UsersRouting} from './users.routing';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {SharedModule} from '../shared/shared.module';
import {UsersBlService} from './users-bl.service';
import {UsersListComponent} from "./components/users-list.component";
import {UsersAddEditComponent} from './components/users-add-edit.component';

@NgModule({
  imports: [RouterModule, UsersRouting.routes, CommonModule, FormsModule,
    SharedModule],
  exports: [],
  declarations: [UsersRouting.components, UsersComponent, UsersListComponent, UsersAddEditComponent],
  providers: [UsersBlService],
})
export class UsersModule {
}
