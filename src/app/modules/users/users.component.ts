import {UsersBlService} from './users-bl.service';
import {Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {User} from '../../shared/classes/user.model';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {ModeTypeEnum} from '../../shared/enums/mode-type.enum';
import {MessageService} from 'primeng/api';
import {AppBlService} from '../../app-bl.service';
import {Subscription} from 'rxjs/internal/Subscription';
import {Observable} from 'rxjs';
import {Constants} from '../../shared/constants';

@Component({
  template: `
    
    <page-header [title]="USERS" [isShowAddBtn]="!userModeType"
                 (addClicked)="changeAddEditMode(modeTypes.add)"></page-header>

    <users-add-edit *ngIf="userModeType == modeTypes.add"
                    [mode]="userModeType"
                    (doneClicked)="saveUserChanges($event)"
                    (backClicked)="changeAddEditMode(null)"></users-add-edit>


    <app-users-list [users]="users$ | async"
                    (editClicked)="editClicked($event)"
                    (deleteClicked)="deleteClicked($event)"></app-users-list>


    <ng-template #templateAddEdit>
      <div class="modal-header">
        <h4 class="modal-title pull-left">Edit User</h4>
        <button type="button" class="close pull-right" aria-label="Close" (click)="modalRef.hide()">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" *ngIf="userModeType === modeTypes.edit">
        <users-add-edit *ngIf="userModeType == modeTypes.edit"
                        [user]="userOnEdit" [mode]="userModeType"
                        (doneClicked)="saveUserChanges($event)"
                        (backClicked)="changeAddEditMode(null)"></users-add-edit>
      </div>
    </ng-template>


    <ng-template #templateDelete>
      <popup-are-you-sure *ngIf="userOnDelete" [id]="userOnDelete.id" [typeData]="USER"
                          (confirm)="confirm()" (decline)="decline()"></popup-are-you-sure>
    </ng-template>


  `,
  styles: []
})
export class UsersComponent implements OnInit, OnDestroy {
  userOnEdit: User;
  userOnDelete: User;
  userModeType: ModeTypeEnum;
  modeTypes = ModeTypeEnum;
  modalRef: BsModalRef;
  @ViewChild('templateAddEdit') templateAddEdit;
  @ViewChild('templateDelete') templateDelete;

  USERS: string = Constants.USERS;
  USER: string = Constants.USER;

  _users_subsc: Subscription;
  _userMode_subsc: Subscription;
  _userOnEdit_subsc: Subscription;
  _userOnDelete_subsc: Subscription;
  onHide_subsc: Subscription;

  users$: Observable<User[]>;

  constructor(public usersBlService: UsersBlService,
              private appBlService: AppBlService,
              private modalService: BsModalService,
              private  messageService: MessageService) {
  }

  ngOnInit() {

    this.users$ = this.appBlService.users;

    // when users list changing -> update local storage
    this._users_subsc = this.appBlService._users.subscribe(users => {
      this.appBlService.setUsersLocalStorage(users);
    });

    this._userMode_subsc = this.usersBlService._userMode.subscribe((status: ModeTypeEnum) => {
      this.userModeType = status;
    });
    this._userOnEdit_subsc = this.usersBlService._userOnEdit.subscribe((user: User) => {
      this.userOnEdit = user;
    });
    this._userOnDelete_subsc = this.usersBlService._userOnDelete.subscribe((user: User) => {
      this.userOnDelete = user;
    });

    this.onHide_subsc = this.modalService.onHide.subscribe(() => {
      this.usersBlService.updateUserMode(null);
    });
  }

  ngOnDestroy() {
    this._users_subsc.unsubscribe();
    this._userMode_subsc.unsubscribe();
    this._userOnEdit_subsc.unsubscribe();
    this._userOnDelete_subsc.unsubscribe();
    this.onHide_subsc.unsubscribe();
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  editClicked(user: User) {
    this.usersBlService.updateUserOnEdit(user);
    this.usersBlService.updateUserMode(ModeTypeEnum.edit);
    this.openModal(this.templateAddEdit);
  }

  deleteClicked(user: User) {
    this.usersBlService.updateUserOnDelete(user);
    this.openModal(this.templateDelete);
  }

  saveUserChanges(user: User) {
    if (!this.usersBlService.isUserValid(user)) {
      this.messageService.add({severity: 'error', summary: 'User not valid', detail: 'not valid'});
      return;
    }
    if (this.userModeType == ModeTypeEnum.edit) {
      this.modalRef.hide();
    }
    this.usersBlService.preformAddEditUser(user);

  }

  changeAddEditMode(status: ModeTypeEnum) {
    // if on edit mode => close
    if (this.userModeType == ModeTypeEnum.edit)
      this.modalRef.hide();
    this.usersBlService.updateUserMode(status);
  }

  confirm() {
    this.usersBlService.preformUserDelete(this.userOnDelete);
    this.modalRef.hide();
  }

  decline() {
    // this.message = 'Declined!';
    this.modalRef.hide();
    this.usersBlService.updateUserOnDelete(null);
  }
}
