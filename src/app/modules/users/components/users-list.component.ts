import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild} from '@angular/core';
import {MatSort, MatTableDataSource} from '@angular/material';
import {User} from '../../../shared/classes/user.model';
import {Constants} from '../../../shared/constants';

@Component({
  selector: 'app-users-list',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div class="dataTable">
      <table mat-table [dataSource]="dataSource" matSort class="mat-elevation-z8">

        <!-- Id Column -->
        <ng-container matColumnDef="id">
          <th mat-header-cell *matHeaderCellDef mat-sort-header> Id</th>
          <td mat-cell *matCellDef="let element">{{element.id}}</td>
        </ng-container>

        <!-- Name Column -->
        <ng-container matColumnDef="name">
          <th mat-header-cell *matHeaderCellDef mat-sort-header> Name</th>
          <td mat-cell *matCellDef="let element"><a routerLink="/orders/{{element.id}}">{{element.name}}</a></td>
        </ng-container>

        <!-- Star Column -->
        <ng-container matColumnDef="star" stickyEnd>
          <th mat-header-cell *matHeaderCellDef></th>
          <td class="btnArea" mat-cell *matCellDef="let element">
            <i class="fa fa-edit" [tooltip]="editUser" (click)="editClicked.emit(element)"></i>
            <i class="fa fa-trash" [tooltip]="deleteUser" (click)="deleteClicked.emit(element)"></i>
          </td>
        </ng-container>


        <tr mat-header-row *matHeaderRowDef="displayedColumns"></tr>
        <tr mat-row *matRowDef="let row; columns: displayedColumns;"></tr>
      </table>
    </div>
  `,
  styles: [`

  `]
})
export class UsersListComponent implements OnInit, OnChanges {
  displayedColumns: string[] = ['id', 'name', 'star'];
  dataSource;
  @Output() editClicked: EventEmitter<User> = new EventEmitter<User>();
  @Output() deleteClicked: EventEmitter<User> = new EventEmitter<User>();
  @Input() users: User[];

  public editUser: string = Constants.EDIT_USER;
  public deleteUser: string = Constants.DELETE_USER;

  @ViewChild(MatSort)
  sort: MatSort;

  constructor() {
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.initDataSource();
  }

  initDataSource(): void {
    this.dataSource = new MatTableDataSource(this.users);
    this.dataSource.sort = this.sort;
  }


}
