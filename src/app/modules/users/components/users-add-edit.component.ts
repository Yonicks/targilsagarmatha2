import {Component, OnInit, Output, Input, EventEmitter, OnChanges, ChangeDetectionStrategy} from "@angular/core";
import {User} from "../../../shared/classes/user.model";
import {ModeTypeEnum} from "../../../shared/enums/mode-type.enum";
import * as _ from 'lodash';

@Component({
  selector: 'users-add-edit',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div class="addUserArea py-5">
      <button class="btn btn-info" (click)="backClicked.emit()">BACK</button>
      <h1>{{title}}-user</h1>

      <form class="example-form">
        <mat-form-field class="example-full-width">
          <input matInput placeholder="Id" disabled [(ngModel)]="userMock.id" name="id">
        </mat-form-field>

        <mat-form-field class="example-full-width">
          <input matInput placeholder="Name" [(ngModel)]="userMock.name" name="name"/>
        </mat-form-field>
      </form>

      <button class="btn btn-primary" (click)="doneClicked.emit(userMock)"
              [class.disabled]="userMock.name.length < 4">Save
      </button>
    </div>
  `,
  styles: [`
    .example-form {
      min-width: 150px;
      max-width: 500px;
      width: 100%;
    }

    .example-full-width {
      width: 100%;
    }
  `]
})
export class UsersAddEditComponent implements OnInit, OnChanges {
  @Output() backClicked = new EventEmitter<any>();
  @Output() doneClicked = new EventEmitter<User>();
  @Input() user: User;
  userMock: User;
  @Input() mode: ModeTypeEnum;
  title: string;

  constructor() {
  }

  ngOnInit() {
    if (this.mode == ModeTypeEnum.add) {
      this.userMock = new User();
    } else {
      this.userMock = _.cloneDeep(this.user);
    }
  }

  ngOnChanges(changes) {
    this.title = this.mode.toString();
  }

}
