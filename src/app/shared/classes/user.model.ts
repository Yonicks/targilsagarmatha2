import { Guid } from "./guid";

export class User {
  id: string;
  name: string;

  constructor() {
    this.id = Guid.newGuid().substr(0, 8);;
    this.name = Guid.newGuid().substr(0, 6);
  }
}
