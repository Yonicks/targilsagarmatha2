import {Guid} from "./guid";
import {User} from "./user.model";

export class Order {
  id: string;
  name: string;
  userId: string;
  user?: User;

  constructor() {
    this.id = Guid.newGuid().substr(0, 8);
    this.name = Guid.newGuid().substr(0, 6);
  }
}
