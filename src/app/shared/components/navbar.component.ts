import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-navbar",
  template: `.
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" [routerLink]="['home']">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" [routerLink]="['users']">Users</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" [routerLink]="['orders']">Orders</a>
        </li>
      </ul>
    </div>
  </nav>
  `,
  styles: []
})
export class NavbarComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
