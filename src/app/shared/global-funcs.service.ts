import { Injectable } from '@angular/core';
import { ConfigService } from '../modules/core/services/config.service';

@Injectable()
export class GlobalFuncsService {
    isProd: boolean;
    constructor(private configService: ConfigService) {
        this.isProd = this.configService.getConfiguration().isProd;
    }
}
