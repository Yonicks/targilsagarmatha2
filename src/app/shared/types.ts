export namespace types {
    export type CallBack = (() => void);
}
