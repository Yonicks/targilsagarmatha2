export class Configuration {
    constructor(
        public version: string,
        public isProd: boolean,
        public data: any,
        public baseUrl: string) {
    }
}
