import { ModuleWithProviders } from "@angular/compiler/src/core";


export interface IRouting {
    routes: ModuleWithProviders,
    components: any[],
}