export class EnsureModuleLoadedOnceGuard {
    constructor(targetModule: any) {
        if (targetModule) {
            throw new Error(`${targetModule.constructor.name} has alredy been loaded. Import this module in the AppModule only.`)
        }
    }
}