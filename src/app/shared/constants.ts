export class Constants {
  public static readonly appName = 'TARGIL';

  public static readonly LS_USERS = 'LOCAL_STORAGE_USERS';
  public static readonly LS_ORDERS = 'LOCAL_STORAGE_ORDERS';


  // TEXT
  public static readonly EDIT_USER = 'עריכת משתמש';
  public static readonly DELETE_USER = 'מחיקת משתמש';
  public static readonly EDIT_ORDER = 'עריכת הזמנה';
  public static readonly DELETE_ORDER = 'מחיקת הזמנה';
  public static readonly YES = 'Yes';
  public static readonly NO = 'No';


  public static readonly ARE_YOU_SURE_DELETE = 'confirm delete';


  public static readonly ORDERS = 'orders';
  public static readonly ORDER = 'order';
  public static readonly USERS = 'users';
  public static readonly USER = 'user';


  // APIS

  public static readonly API_GET_EXAMPLE_DATA = 'getExampleData/';
}
