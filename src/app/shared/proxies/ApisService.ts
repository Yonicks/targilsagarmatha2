import { Injectable } from '@angular/core';
import { HttpService } from '../../modules/core/services/http.service';
import { GlobalFuncsService } from '../global-funcs.service';
import { Observable } from 'rxjs';
import { Constants } from '../constants';
import { IExample } from '../interfaces/IExample';
import { map } from 'rxjs/operators';

@Injectable()
export class ApisService {

    constructor(private _httpService: HttpService, private globalFuncsService: GlobalFuncsService) { }

    getExampleData(someId: string): Observable<IExample[]> {
        return this._httpService.get<IExample[]>(Constants.API_GET_EXAMPLE_DATA + someId)
            .pipe(res => res);
    }

    postExampleData(someId: string): Observable<IExample[]> {
        return this._httpService.post<IExample[]>(someId, Constants.API_GET_EXAMPLE_DATA)
            .pipe(res => res);
    }


    // ASYNC

    async getExampleListAsync(id: string): Promise<any> {
        const res = await this._httpService.getAsync(Constants.API_GET_EXAMPLE_DATA + id);
        return this.responseDataExtracter(res);
    }

    private responseDataExtracter(response: any) {
        try {
            return JSON.parse(response._body);
        } catch (error) {
            return {};
        }
    }
}
