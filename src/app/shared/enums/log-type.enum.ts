export enum LogType {
    debug = 'debug',
    error = 'error',
    warning = 'warning',
}
