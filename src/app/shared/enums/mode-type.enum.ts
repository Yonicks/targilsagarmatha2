export enum ModeTypeEnum {
  add = 'add',
  edit = 'edit',
  delete = 'delete',
}
