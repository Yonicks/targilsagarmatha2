import { Routes, RouterModule } from "@angular/router";
import { IRouting } from "./shared/interfaces/IRouting";
import { NavbarComponent } from "./shared/components/navbar.component";

const routes: Routes = [
  { path: 'home', loadChildren: '../app/modules/home/home.module#HomeModule' },
  { path: 'users', loadChildren: '../app/modules/users/users.module#UsersModule' },
  { path: 'orders', loadChildren: '../app/modules/orders/orders.module#OrdersModule' },
  { path: "", redirectTo: "home", pathMatch: "full" },
  { path: "**", redirectTo: "" }
];

export const appRouting: IRouting = {
  routes: RouterModule.forRoot(routes, { useHash: true }),
  components: [NavbarComponent]
};
