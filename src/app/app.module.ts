import { AppBlService } from './app-bl.service';

import { NgModule } from "@angular/core";

import { AppComponent } from "./app.component";
import { CoreModule } from "./modules/core/core.module";
import { NavbarComponent } from './shared/components/navbar.component';
import { appRouting } from "./app-routing.module";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import {ToastModule} from "primeng/toast";
import {MessageService} from "primeng/api";
import {AngularFontAwesomeModule} from "angular-font-awesome";

@NgModule({
  declarations: [appRouting.components, AppComponent, NavbarComponent],
  imports: [
    CoreModule,
    appRouting.routes,
    BrowserAnimationsModule,
    ToastModule,
    AngularFontAwesomeModule,
  ],
  providers: [AppBlService, MessageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
